<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$id=$_REQUEST['user_id'];
$user_name=$_REQUEST['user_name'];
$phone_number = $_REQUEST['phone_number'];
$date = $_REQUEST['date'];

if (CModule::IncludeModule("iblock")) {

    $el = new CIBlockElement;

    $arProps = array();
    $arProps[17] = $date;
    $arProps[25] = $id;
    $arProps[26] = $user_name;
    $arProps[27] = $phone_number;
    if($_FILES['file']['tmp_name'] != ""){
        $arProps[28] =  CFile::MakeFileArray($_FILES['file']['tmp_name']);
    }
    $arLoadProductArray = Array(
//"MODIFIED_BY" => $USER->GetID(), //ОБъект пользователя
        "IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
        "IBLOCK_ID" => 6, //ID инфоблока
        "PROPERTY_VALUES"=> $arProps, //Свойства
        "NAME" => 'Предзаказ на новую коллекцию клиента '.$user_name, //Имя элемента
        "ACTIVE" => "Y", // Активность элемента
    );
//проверка на успешную работу функции, возвр id добьавл элемента
    if ($PRODUCT_ID = $el->Add($arLoadProductArray))
        echo '1';
}
?>