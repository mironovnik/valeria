<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Где купить");
?>
<div id="page">
    <div class="container">
        <h1>Где купить</h1>
        <img src="/local/templates/valeria_template/img/map.png" alt="">

        <div class="flex contact">
            <div>
                <h2 class="pb">Магазины</h2>
                <p><b>Фирменный магазин</b><br/>
                    Московская обл., г. Коломна, ул. Левшина, д. 21<br/>
                    Пн–Пт: 9:00-17:00<br/>
                    <br/>
                    <b>Магазин "Валерия"</b><br/>
                    Московская обл., г. Коломна, ул. Октябрьской революции<br/>
                    ТК "Светофор", д. 404, пав. 200<br/>
                    <br/>
                    <b>Магазин "Изобель"</b><br/>
                    г. Москва, ул. Стромынка, д. 5<br/>
                    +7 (499) 748 0219, +7 (499) 748 0221<br/>
                    www.charmell.ru</p>
            </div>
            <div>
                <h2 class="pb">Официальные дилеры</h2>
                <p><b>ООО ТД "Аудаче"</b><br/>
                    г. Москва, ул. Верхняя Красносельская, д. 2<br/>
                    +7 (925) 502 4248<br/>
                    www.audace.ru<br/>
                    <br/>
                    <b>Компания "Дарси Трейд"</b><br/>
                    г. Москва, ул. Краснобогатырская, д. 89<br/>
                    +7 (495) 646 2883<br/>
                    www.darsitrade.ru<br/>
                    <br/>
                    <b>ООО "Бельконста"</b><br/>
                    г. Санкт-Петербург, просп. Лиговский, д. 50, корп. 11<br/>
                    +7 (800) 505 9518<br/>
                    www.belconsta.ru
                </p>

            </div>
        </div>
    </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
$APPLICATION->SetTitle("about");
?>