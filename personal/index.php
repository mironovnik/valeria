<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?><?
if(!$USER->isAuthorized())
{
    LocalRedirect(SITE_DIR.'auth/');
}
else {?>

	<div id="page" class="lk">
		<div class="container">
			<h1>Личный кабинет</h1>

			<div class="w1100">
				<div class="select-links">
					<div class="flex space-between midd">
						<a href="#" class="act" data-arial=".tab" data-for=".s1">АКТУАЛЬНАЯ КОЛЛЕКЦИЯ</a>
						<a href="#"  data-arial=".tab" data-for=".s2">ИСТОРИЯ ЗАКАЗОВ</a>
						<a href="#" data-arial=".tab" data-for=".s3">ПРЕДЗАКАЗ НА НОВУЮ КОЛЛЕКЦИЮ</a>
					</div>
				</div>


				<div class="toggle-links">
					<hr>
					<a href="#" class="act">АКТУАЛЬНАЯ КОЛЛЕКЦИЯ</a>
					<div class="tab s1 act">
						<div>
							<img src="<?=SITE_TEMPLATE_PATH.'/img/download.png'?>" alt="">
                            <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"collection", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "10",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "COLLECTIONS",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "Файл коллекции",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "collection"
	),
	false
);?>
							<form id="actual_form">
								<?
									global $USER;
									$user = new CUser;
									$arUser = $user->GetByID($USER->GetID())->Fetch();
								?>
								<input type="hidden" name="user_id" value="<?=$arUser['ID'];?>">
								<input type="hidden" name="date" value="<?=date("d.m.Y");?>" >
								<input type="hidden" name="user_name" value="<?=$arUser['NAME'].' '.$arUser['LAST_NAME'];?>">
								<input type="hidden" name="phone_number" value="<?=$arUser['PERSONAL_PHONE'];?>">
								<input type="file"  name="file" value="ПРИКРЕПИТЬ ФАЙЛ">
								<input type="submit" class="btn">
							</form>
						</div>
					</div>
					<hr>
					<a href="">ИСТОРИЯ ЗАКАЗОВ</a>
                    <?$APPLICATION->IncludeComponent("bitrix:news.list", "history_order", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                        "DISPLAY_DATE" => "N",	// Выводить дату элемента
                        "DISPLAY_NAME" => "N",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "FIELD_CODE" => array(	// Поля
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",	// Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "6",	// Код информационного блока
                        "IBLOCK_TYPE" => "HISTORY",	// Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "20",	// Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",	// Название категорий
                        "PARENT_SECTION" => "",	// ID раздела
                        "PARENT_SECTION_CODE" => "",	// Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "DATA_ORDER",
                            1 => "ID_DELIVERY",
                            2 => "NUMBER_ORDER",
                            3 => "STATUS",
                            4 => "SUMM_ORDER",
                            5 => "",
                        ),
                        "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
                        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                        "SHOW_404" => "N",	// Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
                    ),
                        false
                    );?>
					<hr>
					<a href="#">ПРЕДЗАКАЗ НА НОВУЮ КОЛЛЕКЦИЮ</a>
					<div class="tab s3">
						<div>
							<img src="<?=SITE_TEMPLATE_PATH.'/img/download.png'?>" alt="">
							<a href="" class="btn">СКАЧАТЬ</a>
							<form id="predorder_form">
                                <?
                                global $USER;
                                $user = new CUser;
                                $arUser = $user->GetByID($USER->GetID())->Fetch();
                                ?>
								<input type="hidden" name="user_id" value="<?=$arUser['ID'];?>">
								<input type="hidden" name="date" value="<?=date("d.m.Y");?>" >
								<input type="hidden" name="user_name" value="<?=$arUser['NAME'].' '.$arUser['LAST_NAME'];?>">
								<input type="hidden" name="phone_number" value="<?=$arUser['PERSONAL_PHONE'];?>">
								<input type="file"  name="file" value="ПРИКРЕПИТЬ ФАЙЛ">
								<input type="submit" class="btn">
							</form>
						</div>
					</div>
					<hr>
				</div>

			</div>

		</div>
	</div>

	</div>
<? } ?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>