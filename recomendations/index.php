<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Рекомендации по выбору");
?>
<div id="page" class="razmer">
    <div class="container">
        <h1>Рекомедациия по выбору</h1>
        <div class="w770 text-center">
            <h2 class="pb">Как правильно выбрать</h2>
            <p>
                Покупая нижнее белье, будь то в подарок или для себя, женщине необходимо знать точный размер, потому что нет ничего хуже неправильно подобранного белья, которое нельзя позднее поменять (нижнее белье обмену и возврату не подлежит). И если для маленькой груди, консультант с легкостью может подобрать подходящую модель, то особенно нужно быть внимательным, выбирая белье для пышных дам или дам с нестандартной фигурой. Например, зачастую бывают случаи с непропорциональной фигурой, поэтому необходимо знать свои объемы.
            </p>
        </div>
        <div class="grid col3">
            <div><img src="/local/templates/valeria_template/img/m1.png" alt=""></div>
            <div><img src="/local/templates/valeria_template/img/m2.png" alt=""></div>
            <div><img src="/local/templates/valeria_template/img/m3.png" alt=""></div>
        </div>
        <div class="w770 text-center">
            <p>Для того, чтобы определить свой размер, необходимо вооружиться сантиметровой лентой. Нужно измерить четыре параметра:</p>
        </div>
        <div class="w1180 steps">
            <div class="flex text-center space-around">
                <div>
                    <div class="kr">1</div>
                    <p>Обхват талии</p>
                </div>
                <div>
                    <div class="kr">2</div>
                    <p>Обхват бедер</p>
                </div>
                <div>
                    <div class="kr">3</div>
                    <p>Обхват под грудью</p>
                </div>
                <div>
                    <div class="kr">4</div>
                    <p>Обхват груди по высшей</br>точке выступа</p>
                </div>
            </div>
        </div>
        <div class="w770 text-center">
            <p>Первые два параметра необходимы для определения размера трусов (смотрите Таблица 2), последние два – для бюстгальтера.<br/>
                <br/>
                Размер бюстгальтера определяется двумя составными: цифра и буква. Цифра – это обхват груди. Чтобы определить букву, нужно отнять величину параметра 4 от величины параметра 3, затем результат сверить в Таблице 1.</p>
        </div>

        <div class="grid col2 text-center">
            <div>
                <p>Таблица 1. <br/>
                    Размерная сетка для бюстгальтера</p>
                <table>
                    <tr>
                        <th>A</th>
                        <th>B</th>
                        <th>C</th>
                        <th>D</th>
                        <th>E</th>
                        <th>F</th>
                        <th>G</th>
                        <th>H</th>
                        <th>I</th>
                        <th>J</th>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>14</td>
                        <td>16</td>
                        <td>18</td>
                        <td>20</td>
                        <td>22</td>
                        <td>24</td>
                        <td>26</td>
                        <td>28</td>
                        <td>30</td>
                    </tr>
                </table>
            </div>
            <div>
                <p>Таблица 2. <br/>
                    Размерная сетка для трусиков, боди, сорочек и т.п.</p>
                <table>
                    <tr>
                        <th>EUR</th>
                        <th>RUS</th>
                        <th>ITALY</th>
                        <th>Стандарт</th>
                        <th>Обхват талии,см</th>
                        <th>Обхват бедер,см</th>
                    </tr>
                    <tr>
                        <td>34-36</td>
                        <td>40-42 </td>
                        <td>1</td>
                        <td>XS</td>
                        <td>53-57</td>
                        <td>83-87</td>
                    </tr>
                    <tr>
                        <td>38</td>
                        <td>44</td>
                        <td>2</td>
                        <td>S</td>
                        <td>58-62</td>
                        <td>88-92</td>
                    </tr>
                    <tr>
                        <td>40</td>
                        <td>46</td>
                        <td>3</td>
                        <td>M</td>
                        <td>63-67</td>
                        <td>93-97</td>
                    </tr>
                    <tr>
                        <td>42</td>
                        <td>48</td>
                        <td>4</td>
                        <td>L</td>
                        <td>68-72</td>
                        <td>98-102</td>
                    </tr>
                    <tr>
                        <td>44</td>
                        <td>50</td>
                        <td>5</td>
                        <td>XL</td>
                        <td>73-77</td>
                        <td>103-107</td>
                    </tr>
                    <tr>
                        <td>46</td>
                        <td>52</td>
                        <td>6</td>
                        <td>2L</td>
                        <td>78-82</td>
                        <td>108-112</td>
                    </tr>
                    <tr>
                        <td>48</td>
                        <td>54</td>
                        <td>7</td>
                        <td>3XL</td>
                        <td>83-87</td>
                        <td>113-117</td>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td>56</td>
                        <td>8</td>
                        <td>4XL</td>
                        <td>88-92</td>
                        <td>118-122</td>
                    </tr>
                    <tr>
                        <td>52</td>
                        <td>58</td>
                        <td>9</td>
                        <td>5XL</td>
                        <td>93-97</td>
                        <td>123-127</td>
                    </tr>
                    <tr>
                        <td>54</td>
                        <td>60</td>
                        <td>10</td>
                        <td>6XL</td>
                        <td>98-102</td>
                        <td>128-132</td>
                    </tr>
                    <tr>
                        <td>56</td>
                        <td>62</td>
                        <td>11</td>
                        <td>7XL</td>
                        <td>103-107</td>
                        <td>133-137</td>
                    </tr>
                </table>
            </div>

        </div>
    </div>

</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
$APPLICATION->SetTitle("about");
?>