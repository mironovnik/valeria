 //const menuEight = $('.menuEight');

 $('.menuEight').click(function() {
        	$(this).toggleClass('clickMenuEight');
        	//$('header .flex').toggleClass('act');
        	$('nav').toggleClass('act');
        });

        $('body').click(function(e) {
        	if ($('nav').hasClass('act') && (!($(e.target).parents('nav').length) || $(e.target).parent('header').length )) {
                //$('.menuEight').click();
        	}
        });

        $('.close-nav').click(function() {
            $('.menuEight').click();
        });

        $('*[data-for]').click(function(e) {
        	e.preventDefault();
        	$(this).parent().find('*').removeClass('act');
   			$($(this).attr('data-arial')).removeClass('act');
   			$($(this).attr('data-for')).addClass('act');
        	$(this).addClass('act');
        });

        $('.toggle-links > a').click(function(e) {
        	e.preventDefault();
        	$(this).toggleClass('act');
        	$(this).next().toggleClass('act');
        });

        $('.label-arial label').click(function() {
        	$(this).parent().find('label').removeClass('act');
        	$(this).addClass('act');
        });

        $('.legend .toggle').click(function() {
        	if ($(this).hasClass('act')) {
        		$(this).removeClass('act');
        		$('table.history tr').removeClass('act');
        		$(this).text('Раскрыть')
        	} else {
        		$('table.history tr').addClass('act');
        		$(this).addClass('act');
        		$(this).text('Закрыть');
        	}
        });
 $('#predorder_form').on('submit', function(e){
     e.preventDefault();
     var $that = $(this),
         formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
     $.ajax({
         contentType: false, // важно - убираем форматирование данных по умолчанию
         processData: false, // важно - убираем преобразование строк по умолчанию
         data: formData,
         url: '/add_order/addPreOrder.php', // путь к php-обработчику
         type: 'POST', // метод передачи данных
         dataType: 'json', // тип ожидаемых данных в ответе
         success: function(json){
             if(json){
                 // тут что-то делаем с полученным результатом
             }
         }
     });
 });
 $('#actual_form').on('submit', function(e){
     e.preventDefault();
     var $that = $(this),
         formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
     $.ajax({
         contentType: false, // важно - убираем форматирование данных по умолчанию
         processData: false, // важно - убираем преобразование строк по умолчанию
         data: formData,
         url: '/add_order/addActualOrder.php', // путь к php-обработчику
         type: 'POST', // метод передачи данных
         dataType: 'json', // тип ожидаемых данных в ответе
         success: function(json){
             if(json){
                 // тут что-то делаем с полученным результатом
             }
         }
     });
 });