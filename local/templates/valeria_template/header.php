<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle()?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?=SITE_TEMPLATE_PATH."/img/icon.png"?>" sizes="32x32">
    <script src="<?=SITE_TEMPLATE_PATH."/js/jquery-3.3.1.min.js"?>"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH."/js/jquery.easing.1.3.min.js"?>"></script>
    <script src="<?=SITE_TEMPLATE_PATH."/js/custom.js"?>"></script>
    <?$APPLICATION->ShowHead()?>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/css/style.css"?>">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/custom.css"?>">



</head>
<?$APPLICATION->ShowPanel()?>
<body>
<header>
    <div class="container">
        <div class="flex space-between midd">
            <div class="f25 logo">
                <a class="custom-logo-link" href="/">
                    <img src="<? echo SITE_TEMPLATE_PATH.'/img/logo.png';?> " alt="">
                </a>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "main_menu", Array(
            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
            "DELAY" => "N",	// Откладывать выполнение шаблона меню
            "MAX_LEVEL" => "1",	// Уровень вложенности меню
            "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
            0 => "",
            ),
            "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
            "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
            "ROOT_MENU_TYPE" => "header",	// Тип меню для первого уровня
            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
            false
            );?>
            <div class="loginbar f25 dnm">
            <?
            global $USER;
            if(!$USER->isAuthorized()){ ?>
                <a href="/auth/?page=reg">Регистрация</a>
                <span>|</span>
                <a href="/auth/?page=enter">Вход</a>
            <?}
            else { ?>
                <a href="/personal/"><?=$USER->GetFullName(); ?></a>
                <span>|</span>
                <a href="<?echo $APPLICATION->GetCurPageParam("logout=yes", array(
                        "login",
                        "logout",
                        "register",
                        "forgot_password",
                        "change_password"));?>">Выйти</a>
            <? } ?>
                <a href="" class="tel">+7 (499) 702 3020</a>
            </div>
            <div class="menuEight">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <a href="/auth/" class="login"><img src="<?=SITE_TEMPLATE_PATH .'/img/login.png';?>" alt=""></a>
        </div>
    </div>
</header>
<nav>
    <div class="close-nav">	</div>
    <?$APPLICATION->IncludeComponent("bitrix:menu", "mobile_menu", Array(
        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
        "DELAY" => "N",	// Откладывать выполнение шаблона меню
        "MAX_LEVEL" => "1",	// Уровень вложенности меню
        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
            0 => "",
        ),
        "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
        "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
        "ROOT_MENU_TYPE" => "mobile",	// Тип меню для первого уровня
        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
    ),
        false
    );?>
</nav>

