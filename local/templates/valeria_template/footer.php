<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
?>
<footer>
    <div class="container">
        <a class="custom-logo-link">
            <img src="<?=SITE_TEMPLATE_PATH .'/img/logo.png';?>" alt="">
        </a>
        <div class="flex flex-center">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "footer_1",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "footer_2",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
            <div class="socs">
                <p>Мы в соц сетях</p>
                <a href="" target="_blank"><img src="<?=SITE_TEMPLATE_PATH .'/img/soc1.png';?>" alt=""></a>
                <a href="" target="_blank"><img src="<?=SITE_TEMPLATE_PATH .'/img/soc2.png';?>" alt=""></a>
            </div>
        </div>
    </div>
</footer>

<div class="container">
    <p class="copyright">2018 © Valeria lingerie</p>
</div>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH."/js/custom.js"?>"></script>
</body>
</html>
