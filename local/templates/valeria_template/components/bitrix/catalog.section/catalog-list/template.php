<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);

?>

<div id="page">
    <div class="container">
        <h1><??></h1>
        <div class="grid moda">
        <?foreach($arResult["ITEMS"] as $arItem ):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="">
                <div class="layer">
                    <h4><?=$arItem["NAME"]?></h4>
                    <span><?=$arItem["PROPERTIES"]["ARTICUL"]["VALUE"]?></span>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn">смотреть</a>
                </div>
            </div>

        <?endforeach;?>


        </div>
        <a href="/personal/" class="btn w430">ПЕРЕЙТИ К ЗАКАЗУ</a>
    </div>
</div>