<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? foreach ($arResult['ITEMS'] as $arItems) {

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <h3><?= $arItems['NAME'] ?></h3>
        <img src="<?= $arItems['PREVIEW_PICTURE']['SRC'] ?>" alt="">
        <p><?= $arItems['PROPERTIES']['SIZE']['VALUE'] ?></p>
        <a href="<?= $arItems['PROPERTIES']['LINK']['VALUE'] ?>" class="btn" download="">Скачать</a>
    </div>
<? } ?>