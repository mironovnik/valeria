<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$this->setFrameMode(true);

?>

<? foreach ($arResult['ITEMS'] as $arItem) {
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
<section class="banner" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>)">
    <div class="container">
        <div class="flex midd flex-end">
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="ban-block text-center">
                <h1 class="ban-title"><?=$arItem['NAME'];?></h1>
                <p><?=$arItem['PREVIEW_TEXT'];?></p>
                <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE'];?>" class="btn">Смотреть</a>
            </div>
        </div>
    </div>
</section>
<? } ?>

