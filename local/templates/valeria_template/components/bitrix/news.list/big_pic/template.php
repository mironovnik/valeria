<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <section id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="present">
        <div class="container">
            <div class="relat">
                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" class="d1 abs" alt="">
                <div class="ban-block text-center">
                    <h1 class="ban-title"><?= $arItem['NAME']; ?></h1>
                    <p>
                        <?= $arItem['PREVIEW_TEXT']; ?>
                    </p>
                    <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE']; ?>" class="btn">Смотреть</a>
                </div>
                <img src="<?= $arItem['DETAIL_PICTURE']['SRC']; ?>" class="d2" alt="">
            </div>
        </div>
    </section>
<? endforeach; ?>
