<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="tab s2">
    <div>
        <div class="w880">
            <form class="flex midd space-between filter">
                <span>Искать за период от</span>
                <input type="date" placeholder="33">
                <span>до</span>
                <input type="date" placeholder=" ">
                <button class="btn-form">Показать</button>
            </form>

            <table class="history">
                <tr>
                    <th>Номер заказа</th>
                    <th>Дата</th>
                    <th>Сумма</th>
                    <th>Статус</th>
                    <th>Идентификатор отправления</th>
                </tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

        <tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <td><?=$arItem['PROPERTIES']['NUMBER_ORDER']['VALUE']?></td>
            <td><?=$arItem['PROPERTIES']['DATA_ORDER']['VALUE']?></td>
            <td><?=$arItem['PROPERTIES']['SUMM_ORDER']['VALUE']?></td>
            <td><span class="stat1"><b><?=$arItem['PROPERTIES']['STATUS']['VALUE']?></b></span></td>
            <td><span class="stat1"><b><?=$arItem['PROPERTIES']['ID_DELIVERY']['VALUE']?></b></span></td>
        </tr>
<?endforeach;?>
            </table>
            <div class="dn legend">
                <button class="toggle">Раскрыть</button>

                <div class="flex space-around">
                    <div>
                        <span class="stat1"></span>
                        <b>собирается</br>оформляется</b>
                    </div>
                    <div>
                        <span class="stat2"></span>
                        <b>собран</br>отправлен</b>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


