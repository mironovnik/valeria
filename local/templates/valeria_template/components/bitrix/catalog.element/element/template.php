<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="flex space-around sumary" id="page">
    <div class="col1">
        <a href="/catalog/">&lt;  В КАТАЛОГ</a>
    </div>

    <div class="col2">
        <div class="dn">
            <h2 class="text-center"><?= $arResult['NAME']; ?></h2>
            <span class="art"><?= $arResult['PROPERTIES']['ARTICUL']['VALUE']; ?></span>
        </div>
        <img src="<?= $arResult['PREVIEW_PICTURE']['SRC']; ?>" alt="">
    </div>
    <div class="col3">
        <div id="props" class="text-center">
            <h1 class="h2"><?= $arResult['NAME']; ?></h1>
            <span class="art"><?= $arResult['PROPERTIES']['ARTICUL']['VALUE']; ?></span>
            <p class="descript">
                <?= $arResult['PREVIEW_TEXT']; ?></p>
            <hr class="mini">
            <p>Цвет</p>
            <div class="label-arial">
                <? foreach ($arResult['PROPERTIES']['COLOR']['VALUE'] as $key=>$arColor) {?>
                    <? if ($key < 1) { ?>
                    <label for="" class="act"><span style="background-color:<?=$arColor;?>"></span></label>
                    <?} else { ?>
                        <label for=""><span style="background-color:<?=$arColor;?>"></span></label>
                     <? } ?>
                <? } ?>
            </div>

            <hr class="mini">
            <p>BAND SIZE</p>
            <div class="label-arial">
                <? foreach ($arResult['PROPERTIES']['BAND_SIZE']['VALUE'] as  $key=>$arSize) { ?>
                    <? if ($key < 1) { ?>
                        <label for="" class="act"><span><?=$arSize?></span></label>
                    <?} else { ?>
                        <label for=""><span><?=$arSize?></span></label>
                    <? } ?>
                <? } ?>

            </div>

            <p>CUP SIZE</p>
            <div class="label-arial">
                <? foreach ($arResult['PROPERTIES']['CUP_SIZE']['VALUE'] as $arSize) { ?>
                    <label for=""><span><?=$arSize?></span></label>
                <? } ?>


            </div>
            <a href="" class="btn">Предзаказ</a>
        </div>
    </div>

    <div class="flex midd space-between thumbs col4">
        <div class="col1"><img src="http://valeria.weborona.ru/wp-content/uploads/2018/10/t1.png" alt=""></div>
        <div class="col2"><img src="http://valeria.weborona.ru/wp-content/uploads/2018/10/t2.png" alt=""></div>
    </div>

</div>