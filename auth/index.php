<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>
<?
global $USER;
if($USER->isAuthorized()){
    LocalRedirect(SITE_DIR.'personal/');
}
else { ?>


    <div id="login">
        <div class="w860">
            <div class="btns flex">
                <a href="#" class="btn login
                <? if(empty($_GET)) echo 'act';
                elseif ($_GET['page'] == 'enter') echo 'act';?>"
                   data-for=".form1" data-arial="form">Вход</a>
                <a href="#" class="btn reg <? if ($_GET['page'] == 'reg') echo 'act';?> " data-for=".form2" data-arial="form">Регистрация</a>
            </div>
            <h2 class="dn">Вход</h2>
            <? $APPLICATION->IncludeComponent(
                'api:auth.login',
                'login',
                array(
                    "SET_TITLE"       => "Y",
                    "MESS_AUTHORIZED" => "Добро пожаловать на сайт!",
                )
            );
            ?>
            <h2 class="dn">Регистрация</h2>
            <? $APPLICATION->IncludeComponent(
                'api:auth.register',
                'register',
                array(
                    "SET_TITLE"       => "Y",
                    "MESS_AUTHORIZED" => "Добро пожаловать на сайт!",
                )
            );
            ?>
        </div>
    </div>
<? } ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>